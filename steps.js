const { I } = inject();
// Add in your custom step files

Given ('I have entered the reqress endpoint', () => {
   I.amOnPage('https://reqres.in/api/users/10')
   I.wait(3)
   I.see('10-image.jpg')
   I.wait(3)
});

When ('I perform get reqress',() => {
   I.amOnPage('https://reqres.in/img/faces/10-image.jpg')
   I.wait(5)

});

Then ('I should see that the avatar is not null',() => {
   I.seeCurrentUrlEquals('https://reqres.in/img/faces/10-image.jpg')
   I.saveScreenshot('debug.png', true)
   I.wait(5)


});
